//Begin

list<AggregateResult> guidDoppi = [SELECT PED_guid__c, count(id) FROM contact GROUP BY PED_guid__c HAVING count(id) = 2];
set<string> guids = new set<string>();
for(AggregateResult c : guidDoppi)
	guids.add((string)c.get('PED_guid__c'));

list<contact> contactDaBonificare = [SELECT id, PED_guid__c, PPW_PraticaInVolo__c FROM contact WHERE PED_guid__c IN: guids];

set<Id> contactIds = new set<Id>();
map<string, list<contact>> idXguid = new map<string, list<contact>>();

for(contact c : contactDaBonificare){
    if(idXguid.containsKey(c.PED_guid__c))
        idXguid.get(c.PED_guid__c).add(c);
    else {
        idXguid.put(c.PED_guid__c, new List<contact>());
        idXguid.get(c.PED_guid__c).add(c);
    }
    contactIds.add(c.Id);
}

list<User> utentiConContact = [SELECT id, contactid, isactive FROM user WHERE contactid IN: contactIds];

map<id, user> userXcontactId = new map<id, user>();
for(user u : utentiConContact)
    userXcontactId.put(u.contactid, u);

integer siProduttoreNoPED = 0;
integer noProduttoreSiPED = 0;
integer siProduttoreSiPED = 0;
integer noProduttoreNoPED = 0;

for(string s : idXguid.keyset()){
    user uProduttore;
    user uPED;
    
    for(contact c : idXguid.get(s)){
        if(c.PPW_PraticaInVolo__c)
            uProduttore = userXcontactId.get(c.Id);
        else
            uPED = userXcontactId.get(c.Id);
    }
    
    if(uProduttore != null && uPED == null){
        siProduttoreNoPED++;
    }
    else if(uPED != null && uProduttore == null){
        if(uPED.isActive)
        	noProduttoreSiPED++;
    }
    else if(uProduttore != null && uPED != null){
        if(uPED.isActive)
        	siProduttoreSiPED++;
    }
    else if(uProduttore == uPED){
        noProduttoreNoPED++;
    }
}

system.debug('@@@Questo GUID è produttore e NON trovo PED: ' + siProduttoreNoPED);
system.debug('@@@Questo GUID NON è produttore e trovo PED attivo: ' + noProduttoreSiPED);
system.debug('@@@Questo GUID è produttore e trovo anche PED attivo: ' + siProduttoreSiPED);
system.debug('@@@Questo GUID NON è produttore e NON trovo PED: ' + noProduttoreNoPED);